package com.example.dbigaj.superticket;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class EventsListActivity extends AppCompatActivity {

    private static RecyclerView myEventsListView;
    private static TextView emptyView;
    private static Handler handler;
    private Button bLogOut;
    private String uid;
    private static MyEventsListAdapter adapter;
    private static ArrayList<Event> events = new ArrayList<>();

    public EventsListActivity() {
        handler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_list);

        uid = getIntent().getStringExtra("uid");

        myEventsListView = (RecyclerView) findViewById(R.id.listMyEvents);
        emptyView = (TextView) findViewById(R.id.emptyView);

        bLogOut = (Button) findViewById(R.id.buttonLogOut);
        bLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uid = null;
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

        searchEvents(getApplicationContext(), uid);
    }

    public static void searchEvents(final Context context, final String id) {
        new Thread() {
            public void run() {
                events = getEvents(context, id);
                if (events == null) {
                    handler.post(new Runnable() {
                        public void run() {
                            myEventsListView.setVisibility(View.GONE);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            myEventsListView.setVisibility(View.VISIBLE);
                            emptyView.setVisibility(View.GONE);
                            Collections.reverse(events);
                            adapter = new MyEventsListAdapter(events);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                            myEventsListView.setLayoutManager(mLayoutManager);
                            myEventsListView.setAdapter(adapter);
                        }
                    });
                }
            }
        }.start();
    }

    public static ArrayList<Event> getEvents(final Context context, final String id) {
        try {
            URL url = new URL(String.format("http://156.17.42.122:8000/roles/?account_id=" + id));
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();

            connection.addRequestProperty("User-Agent", "my-rest-app");

            JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream()));

            Event event;
            ArrayList<Event> events = new ArrayList<>();

            reader.beginArray();
            while (reader.hasNext()) {
                event = new Event();
                reader.beginObject();
                while (reader.hasNext()) {
                    String name = reader.nextName();
                    if (name.equals("eventId")) {
                        event.setEid(reader.nextString());
                    } else if (name.equals("name")) {
                        event.setName(reader.nextString());
                    } else if (name.equals("date")) {
                        event.setTerm(reader.nextString());
                    } else {
                        reader.skipValue();
                    }
                }
                reader.endObject();
                events.add(event);
            }
            reader.endArray();

            reader.close();

            if (connection.getResponseCode() != 200) {
                connection.disconnect();
                return null;
            }
            connection.disconnect();
            return events;
        } catch (Exception e) {
            return null;
        }
    }
}
