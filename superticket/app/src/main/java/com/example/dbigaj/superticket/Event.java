package com.example.dbigaj.superticket;

public class Event {
    private String eid;
    private String name;
    private String description;
    private String term;
    private String address;

    public Event() {

    }

    public Event(String eid, String name, String description, String term, String address) {
        this.eid = eid;
        this.name = name;
        this.description = description;
        this.term = term;
        this.address = address;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
