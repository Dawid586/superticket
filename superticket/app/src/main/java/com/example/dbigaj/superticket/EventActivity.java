package com.example.dbigaj.superticket;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class EventActivity extends AppCompatActivity {
    private static Handler handler;
    private static TextView tvName, tvTerm, tvAddress;
    private static ImageView imgView;
    private Button bScan;
    private String eid;

    public EventActivity() {
        handler = new Handler();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        eid = getIntent().getStringExtra("eid");

        tvName = (TextView) findViewById(R.id.textViewNameInfo);
        tvTerm = (TextView) findViewById(R.id.textViewTermInfo);
        tvAddress = (TextView) findViewById(R.id.textViewAddressInfo);
        bScan = (Button) findViewById(R.id.buttonScan);

        bScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ScannerActivity.class);
                intent.putExtra("eid", eid);
                startActivity(intent);
            }
        });

        check(getApplicationContext(), eid);
    }

    public static void check(final Context context, final String id) {
        new Thread() {
            public void run() {
                final Event event = getEvent(context, id);
                if (event != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            while (tvName == null) {
                            }
                            tvName.setText(event.getName());
                            tvTerm.setText(event.getTerm());
                            tvAddress.setText(event.getAddress());
                        }
                    });
                }
            }
        }.start();
    }

    public static Event getEvent(final Context context, final String id) {
        try {
            URL url = new URL(String.format("http://156.17.42.122:8000/events/" + id));
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();

            connection.addRequestProperty("User-Agent", "my-rest-app");

            JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream()));

            Event event = new Event();
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("name")) {
                    event.setName(reader.nextString());
                } else if (name.equals("startDate")) {
                    event.setTerm(reader.nextString());
                } else if (name.equals("address")) {
                    event.setAddress(reader.nextString());
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();

            reader.close();

            if (connection.getResponseCode() != 200) {
                connection.disconnect();
                return null;
            }
            connection.disconnect();
            return event;
        } catch (Exception e) {
            return null;
        }
    }
}
