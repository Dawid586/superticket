package com.example.dbigaj.superticket;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by DBIGAJ on 2018-03-20.
 */

public class MyEventsListAdapter extends RecyclerView.Adapter<MyEventsListAdapter.MyViewHolder> {
    private Context context;
    private final List<Event> myEvents;

    @Override
    public MyEventsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.events_list_row, parent, false);
        return new MyEventsListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyEventsListAdapter.MyViewHolder holder, int position) {
        holder.e = myEvents.get(position);

        holder.eid = holder.e.getEid();
        holder.name.setText(holder.e.getName());
        holder.datetime.setText(holder.e.getTerm());
    }

    @Override
    public int getItemCount() {
        return myEvents.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView name, datetime;
        private String eid;
        private Event e;

        MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.textViewName);
            datetime = (TextView) view.findViewById(R.id.textViewDateTime);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EventActivity.class);
                    intent.putExtra("eid", eid);
                    context.startActivity(intent);
                }
            });

        }
    }

    public MyEventsListAdapter(List<Event> myEvents) {
        this.myEvents = myEvents;
    }
}
