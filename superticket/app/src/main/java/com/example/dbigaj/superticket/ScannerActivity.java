package com.example.dbigaj.superticket;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ScannerActivity extends AppCompatActivity {
    private SurfaceView scannerSurfaceView;
    private BarcodeDetector barcodeDetector;
    private static CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION_ID = 1001;
    private static Handler handler;
    private static Context context;
    private static String eid;
    private boolean p = false;

    public ScannerActivity() {
        handler = new Handler();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION_ID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        cameraSource.start(scannerSurfaceView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);

        context = this;
        eid = getIntent().getStringExtra("eid");

        scannerSurfaceView = (SurfaceView) findViewById(R.id.scannerSurfaceView);

        barcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build();
        cameraSource = new CameraSource.Builder(this, barcodeDetector).build();

        scannerSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getParent(), new String[]{android.Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION_ID);
                    return;
                }
                try {
                    cameraSource.start(scannerSurfaceView.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes = detections.getDetectedItems();

                if (qrcodes.size() != 0) {
                    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vibrator.vibrate(1000);
                    checkCode(qrcodes.valueAt(0).displayValue, eid);
                }
            }
        });
    }

    public static void checkCode(final String qr, final String id) {
        new Thread() {
            public void run() {
                final String qrCode = isScanned(qr, id);
                if (qrCode.equals("null")) {
                    handler.post(new Runnable() {
                        public void run() {
                            MyDialogFragment dialog = MyDialogFragment.newInstance("Bilet został zeskanowany");
                            dialog.show(((((AppCompatActivity) context).getSupportFragmentManager())), "Dialog");

                        }
                    });
                } else if (qrCode.contains("-")) {
                    handler.post(new Runnable() {
                        public void run() {
                            MyDialogFragment dialog = MyDialogFragment.newInstance("Bilet był już skanowany w terminie " + qrCode);
                            dialog.show(((((AppCompatActivity) context).getSupportFragmentManager())), "Dialog");
                        }
                    });
                } else if (qrCode.contains("!")) {
                    handler.post(new Runnable() {
                        public void run() {
                            MyDialogFragment dialog = MyDialogFragment.newInstance(qrCode);
                            dialog.show(((((AppCompatActivity) context).getSupportFragmentManager())), "Dialog");
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            MyDialogFragment dialog = MyDialogFragment.newInstance("Nie ma takiego biletu!");
                            dialog.show(((((AppCompatActivity) context).getSupportFragmentManager())), "Dialog");
                        }
                    });
                }
            }
        }.start();
    }

    public static String isScanned(final String qr, final String id) {
        try {
            cameraSource.stop();
            URL url = new URL(String.format("http://156.17.42.122:8000/tickets/ticket/scan/" + qr + "/" + id + "/"));
            HttpURLConnection connection =
                    (HttpURLConnection) url.openConnection();

            connection.addRequestProperty("User-Agent", "my-rest-app");

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));

            StringBuilder sb = new StringBuilder(reader.readLine());

            reader.close();

            if (connection.getResponseCode() != 200) {
                connection.disconnect();
                return "Błąd skanowania! Spróbuj ponownie.";
            }
            connection.disconnect();
            return sb.toString();
        } catch (Exception e) {
            return "Błędny bilet!";
        }
    }

    public static class MyDialogFragment extends DialogFragment {

        static private String m;

        static MyDialogFragment newInstance(String message) {
            MyDialogFragment dialog = new MyDialogFragment();
            m = message;
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
            dialogBuilder.setTitle("Info");
            dialogBuilder.setMessage(m);
            dialogBuilder.setPositiveButton("Ok", new Dialog.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int whichButton) {
                    Intent intent = new Intent(getActivity(), EventActivity.class);
                    intent.putExtra("eid", eid);
                    startActivity(intent);
                }
            });
            return dialogBuilder.create();
        }
    }
}
